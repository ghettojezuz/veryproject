#include "stdafx.h"
#include "CppUnitTest.h"
#include  "../veryproject/veryproject.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace veryproject_test
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(Test2to10)
		{
			int number = 100100;
			Assert::AreEqual(perevod2to10(number),36);
		}
		
		TEST_METHOD(Test10to2)
		{
			int number = 36;
			string a = "100100";
			Assert::AreEqual(perevod10to2(number), a);
		}

		TEST_METHOD(Test8to2)
		{
			int number = 17;
			string a = "001111";
			Assert::AreEqual(perevod8to2(number), a);
		}

		TEST_METHOD(Test2to8)
		{
			int number = 10000;
			string a = "20";
			Assert::AreEqual(perevod2to8(number), a);
		}

		TEST_METHOD(Test8to16)
		{
			string st = "55";
			string a = "2D";
			Assert::AreEqual(perevod8to16(st), a);
		}

		TEST_METHOD(Test16to8)
		{
			string st = "2AA";
			string a = "1252";
			Assert::AreEqual(perevod16to8(st), a);
		}

		TEST_METHOD(TestSum)
		{
			string s1 = "100100";
			string s2 = "10001";
			string a = "110101";
			Assert::AreEqual(sum(s1,s2), a);
		}

		TEST_METHOD(TestMul)
		{
			string s1 = "101";
			string s2 = "111";
			string a = "100011";
			Assert::AreEqual(mul(s1, s2), a);
		}

		TEST_METHOD(TestSub)
		{
			int s1 = 100101;
			int s2 = 1001;
			string a = "11100";
			Assert::AreEqual(sub(s1, s2), a);
		}
	};
}